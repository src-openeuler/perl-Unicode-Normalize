%global base_version 1.26
%define mod_name Unicode-Normalize
Name:            perl-%{mod_name}
Version:         1.32
Release:         2
Summary:         Unicode Normalization Forms
License:         GPL-1.0-or-later OR Artistic-1.0-Perl
URL:             https://metacpan.org/release/%{mod_name}
Source0:         https://cpan.metacpan.org/authors/id/K/KH/KHW/%{mod_name}-%{base_version}.tar.gz

Patch6000:	backport-1.26-Update-to-1.32.patch

BuildRequires:   gcc findutils make perl-devel perl-generators perl-interpreter perl-libs
BuildRequires:   perl(Carp) perl(constant) perl(ExtUtils::MakeMaker) >= 6.76 perl(File::Spec)
BuildRequires:   perl(SelectSaver) perl(strict) perl(warnings) perl(Exporter) perl(XSLoader)
Conflicts:       perl < 4:5.22.0-347

%description
This package takes Unicode data of arbitrary forms and canonicalizes it to a standard
representation(Unicode Standard Annex #15).

%package_help

%prep
%autosetup -n %{mod_name}-%{base_version} -p1

# Generate ppport.h which is used since 1.32
perl -MDevel::PPPort \
    -e 'Devel::PPPort::WriteFile() or die "Could not generate ppport.h: $!"'

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 OPTIMIZE="%{optflags}"
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc Changes README
%license LICENSE
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/Unicode

%files help
%{_mandir}/man3/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 1.32-2
- drop useless perl(:MODULE_COMPAT) requirement

* Sat Oct 28 2023 hongjinghao <hongjinghao@huawei.com> - 1.32-1
- Upgrade to 1.32

* Mon Oct 24 2022 yangmingtai <yangmingtai@huawei.com> - 1.28-2
- define mod_name to opitomize the specfile

* Mon Dec 27 2021 guozhaorui<guozhaorui1@huawei.com> - 1.28-1
- update version to 1.28

* Fri Sep 27 2019 chengquan<chengquan3@huawei.com> - 1.26-419
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix spec rule in openeuler

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.26-418
- Package init
